package com.sso.domain;

import java.util.Date;

/**
 * @author lyy
 * @data 2021-6-2 09:52:18
 * @description:
 */
@Data
public class Payload<T> {
    private String id;
    private T userInfo;
    private Date expiration;
}
